# General Library Helper

## Overview
 Sebuah library yang menyediakan fungsi-fungsi yang sering digunakan bertujuan

## Installation
``
composer install ptipduinsuskariau-dev-a/libhelper
``

### Requirements
1. PHP >= 7.1

## Development
### How to Contribute
See [CONTRIBUTE.md](CONTRIBUTE.md)

### Changelog
See [CHANGELOG.md](CHANGELOG.md)

### Contributors
1. Pizaini (pizaini@uin-suska.ac.id)

### Licence
GPL Licence