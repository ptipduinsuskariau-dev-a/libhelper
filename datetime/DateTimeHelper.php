<?php

/**
 * Created by PhpStorm.
 * User: Pizaini <pizaini@uin-suska.ac.id>
 * Date: 29/07/2017
 * Time: 9:56
 */
namespace ptipduinsuskariau\libhelper\datetime;

class DateTimeHelper
{
    /**
     * Tahun-bulan-hari Jam:menit:detik
     */
    const FORMAT_SECOND = 'Y-m-d H:i:s';

    /**
     * Tahun-bulan-hari Jam:menit
     */
    const FORMAT_MINUTE = 'Y-m-d H:i';

    /**
     * Tahun-bulan-hari Jam
     */
    const FORMAT_HOUR = 'Y-m-d H';

    /**
     * Tahun-bulan-hari
     */
    const FORMAT_DATE = 'Y-m-d';

}