<?php
/**
 * Created by Pizaini <pizaini@uin-suska.ac.id>
 * Date: 22/11/2017
 * Time: 10:31
 */

namespace ptipduinsuskariau\libhelper\json;

use ptipduinsuskariau\libhelper\datetime\DateTimeHelper;

class JsonHelper
{
    /**
     * Reformat friendly json for histori terakhir
     * @param string $json
     * @return string
     */
    public static function renderHistoriTerakhir(string $json):string{
        $decodedJson = json_decode($json);
        if(json_last_error() !== JSON_ERROR_NONE){
            return '';
        }
        $tglOnJson= $decodedJson->tgl ?? time();
        $tgl = (new \DateTime())->setTimestamp($tglOnJson)->format(DateTimeHelper::FORMAT_MINUTE);
        $user = $decodedJson->user ?? '(n/a)';
        return $tgl.' by '.$user;
    }
}