<?php
/**
 * Created by Pizaini <pizaini@uin-suska.ac.id>
 * Date: 01/11/2017
 * Time: 18:47
 */

namespace ptipduinsuskariau\libhelper\mysql;


class MySqlJsonHelper
{
    /**
     * Help to convert mysql function: JSON_OBJECT()
     * @param array ...$data
     * @return array
     */
    public static function jsonObject(array $data): array {
        $keyValue = array();
        $param = array();
        $inc = 0;
        try{
            $inc = random_int(0, 9999);
        }catch (\Exception $e){
            //not handled exception
        }
        foreach ($data as $k => $v){
            $key = 'k_'.$inc;
            $val = 'v_'.$inc;
            $keyValue[] = ':'.$key;
            $keyValue[] = ':'.$val;
            $param[$key] = $k;
            $param[$val] = $v;
            $inc++;
        }
        return [
            'keyValue' => implode(',', $keyValue),
            'param' => $param
        ];
    }

    /**
     * Help to convert mysql function: JSON_ARRAY()
     * @param array $data
     * @return array
     */
    public static function jsonArray(array $data): array {
        $keyValue = array();
        $param = array();
        $inc = 0;
        try{
            $inc = random_int(0, 9999);
        }catch (\Exception $e){
            //not handled exception
        }
        foreach ($data as $v){
            $p = 'p_'.$inc;
            $keyValue[] = ':'.$p;
            $param[$p] = $v;
            $inc++;
        }
        return [
            'keyValue' => implode(',', $keyValue),
            'param' => $param
        ];
    }
}